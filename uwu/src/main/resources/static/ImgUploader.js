/*
it is extremely important to throw the script in the html file
at the bottom of your html file as well, otherwise we will
not have credentials for uploading images.

Yes, this is amazing security
*/

function s3upload() {
  let input = document.getElementById("fileUpload");
  let file = input.files;
  let filename = input.value.split(/(\\|\/)/g).pop();
  let fileUrl = null;

  if (file) {
    file = file[0];
    fileUrl = "http://paperproj2.s3-website.us-east-2.amazonaws.com/" + filename;

    console.log(fileUrl)

    s3.upload({ Key: filename, Body: file }, function (err) {
      if (err) {
        console.log("error");
      }
      alert("Successfully Updated profile picture!");
    });
  return fileUrl;
  }
}