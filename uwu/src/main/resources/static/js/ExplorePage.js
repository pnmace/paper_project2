const DOMAIN = 'http://localhost:9876'
let posts = null;

window.onload = function () {
    let quantity = 100; //controls the initial quanity of posts loaded
    getPosts().then((response) => iterator(posts = response, quantity));
    document.getElementById('morePosts').addEventListener('click', function () { quantity += 5; iterator(posts, quantity) }) // controls the increment
    document.getElementById('logout').addEventListener('click', logout);
    document.getElementById('myProfile').addEventListener('click', myProfile);
    document.getElementById('searchButton').addEventListener('click', search);
}

function search() {
    let userSearch = document.getElementById('searchField').value;
    window.location.replace(DOMAIN + '/html/ProfilePage.html?username=' + userSearch);
}

async function myProfile() {
    window.location.replace(DOMAIN + '/html/ProfilePage.html?username=' + sessionStorage.getItem('userLogin'));
}

async function getPosts() {
    posts = await fetch(DOMAIN + '/getAllPosts', {
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
        method: "GET",
    }); //fetches posts from endpoint

    posts = await posts.json();

    return posts;
}

async function getPostComments(post) {
    const responsePayload = await fetch(DOMAIN + '/user/getAllComments', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Access-Control-Allow-Origin': '*' },
        body: JSON.stringify(post),
    });
    const responseJSON = await responsePayload.json();
    return responseJSON
}

async function getPostLikes(post) {
    const responsePayload = await fetch(DOMAIN + '/allLikesByPost', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Access-Control-Allow-Origin': '*' },
        body: JSON.stringify(post),
    });
    const responseJSON = await responsePayload.json();
    return responseJSON
}

async function iterator(posts, quantity) {
    document.getElementById("content_div").innerHTML = "";
    for (let i = 0; i < quantity; i++)
        await eachPost(posts[i], i);
    addCommentEventListeners()
}

async function logout() {
    const responsePayload = await fetch(DOMAIN + '/user/logout', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Access-Control-Allow-Origin': '*' },
    });
    const responseJSON = await responsePayload.json();
    window.location.replace(DOMAIN + "/");
}

//Creates each new post as a div, populates it and appends it to main
async function eachPost(post, ind) {
    if (typeof post === 'undefined') return null

    let comments = await getPostComments(post).then((result) => { return result })
    let likes = await getPostLikes(post).then((result) => { return result })
    console.log(likes)

    let allCommentsHTML = getCommentsHTML(comments)
    let postPicHTML = ''
    if (post.postPic !== null) postPicHTML = `<img class="rounded w-100 h-auto"  src="${post.postPic}" alt="Card image cap">`
    let newPost = document.createElement("div");

    newPost.innerHTML = `<div class ="card card-body rounded shadow w-100">
    <div class ="row">
       <div class = "col-4 align-items-center">
          ${postPicHTML}
       </div>
       <div class = "col-8">
          <div class = "row">
             <div id="postId"></div>
             <div id="postTitle">user: <a href="http://localhost:9876/html/ProfilePage.html?username=${post.myUser.username}">${post.myUser.username}</a>&nbsp;&nbsp;<strong>${post.postHeading}</strong>&nbsp;&nbsp;${post.postTimestamp}</div>
          </div>
          <div class = "row">
             <div id="postBody">${post.postBody}</div>
          </div>
       </div>
    </div>
    <div class="row">
       <div class ="col">
          <div class="form-check">
            <h6 id="numOfLikes-${ind}">Likes: ${likes.length}</h6>
            <button class="btn btn-primary" style="margin-top:10px;margin-bottom:10px;" id="likePost-${ind}">Like Post</button>
          </div>
          <a class="btn btn-primary" data-toggle="collapse" href="#collapse-${ind}" role="button" aria-expanded="false" aria-controls="collapse-${ind}">
             View Comments
          </a>
       </div>
    </div>
    <div class="collapse" id="collapse-${ind}">
       <div class="">
          ${allCommentsHTML}<br>
          <h6>New Comment:</h6><br>
          <textarea id="commentBody-${ind}" name="commentBody-${ind}" rows="4" cols="50" style="resize: none;"></textarea><br><br>
          <button class="btn btn-primary" id="commentID-${ind}">Create Comment</button>
       </div>
    </div>
 </div>`;

    let selection = document.getElementById("content_div");
    selection.appendChild(newPost);

}

function addCommentEventListeners() {
    for (i in posts) {
        let commentSubmitBtn = document.getElementById(`commentID-${i}`)
        commentSubmitBtn.addEventListener('click', submitComment)
        let likeSubmitBtn = document.getElementById(`likePost-${i}`)
        likeSubmitBtn.addEventListener('click', likePost)
        // console.log(likeSubmitBtn)
    }
}

function likePost() {
    let ID = this.getAttribute('id').match(/\d+$/); console.log(ID);
    let post = posts[ID]; console.log(ID[0]);
    let objToSend = {
        "post": post
    }

    let xhttp = new XMLHttpRequest()

    xhttp.onreadystatechange = function () {
        // only when completed and status code == 201 V
        if (xhttp.readyState == 4) {
            if (xhttp.status == 200) {
                let servletResp = JSON.parse(xhttp.responseText)
                console.log(servletResp)
            }
        }
    }

    xhttp.open('post', DOMAIN + '/likePost');
    xhttp.setRequestHeader('Content-Type', 'application/json');
    xhttp.setRequestHeader('Accept', 'application/json');
    xhttp.send(JSON.stringify(objToSend));

}

function submitComment() {
    let ID = this.getAttribute('id').match(/\d+$/);
    let commentBody = document.getElementById(`commentBody-${ID[0]}`).value
    if (commentBody.length < 1) return null
    console.log(commentBody)
    console.log(posts[ID[0]])
    let objToSend = {
        "post": posts[ID[0]],
        "commentBody": commentBody
    }
    createComment(objToSend)

}

function getCommentsHTML(comments) {
    let out = ``
    for (c of comments) {
        out = out.concat(`<div class="card" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">${c.user.username}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${c.commentTimestamp}</h6>
          <p class="card-text">${c.commentBody}</p>
        </div>
      </div>
      `)
    }
    return out
}

async function createComment(commentObj) {
    console.log(commentObj)
    const responsePayload = await fetch(DOMAIN + '/user/createComment', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Access-Control-Allow-Origin': '*', "Access-Control-Allow-Credentials": true },
        body: JSON.stringify(commentObj),
    });
    window.location.replace(DOMAIN + "/explore");
}