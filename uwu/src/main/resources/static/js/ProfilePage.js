const DOMAIN = 'http://localhost:9876'
let posts = null;

//url stuff
let params;
let username;

window.onload = function () {
    params = (new URL(document.location)).searchParams;
    username = params.get('username');//?username=blahblahblah
    getPosts().then((response) => iterator(posts = response)).then();

    // here we want to create these listeners *and* html elements if
    // the current logged in user == username
    if (sessionStorage.getItem('userLogin') == username) {
        document.getElementById('updateUser').addEventListener('click', updateAcc);
        document.getElementById('createPost').addEventListener('click', createPost);
        // else remove the hamburger
    } else {
        document.getElementById('yummyHamburger').remove();
    }

    document.getElementById('logout').addEventListener('click', logout);
    setProfileCard();
}


function updateAcc() {
    s3upload("fileUpload");

    // lol
    let input = document.getElementById("fileUpload");
    let filename = input.value.split(/(\\|\/)/g).pop();
    let pictureUrl = "http://paperproj2.s3-website.us-east-2.amazonaws.com/" + filename;

    let xhttp = new XMLHttpRequest()

    xhttp.onreadystatechange = function () {
        // only when completed and status code == 201 V
        if (xhttp.readyState == 4) {
            if (xhttp.status == 200) {
                let servletResp = JSON.parse(xhttp.responseText)
                console.log(servletResp)
            }
        }
    }

    xhttp.open('post', DOMAIN + '/updateUser')
    xhttp.setRequestHeader('Content-Type', 'application/json')
    xhttp.setRequestHeader('Accept', 'application/json')

    let objToSend = {
        "username": username,
        "userBio": document.getElementById('bio').value,
        "filepath": pictureUrl
    }
    console.log(objToSend)
    xhttp.send(JSON.stringify(objToSend))
}


function setProfileCard() {
    document.getElementById('userNameHere').innerText = username;

    let xhttp = new XMLHttpRequest()

    xhttp.onreadystatechange = function () {
        console.log("awoo");
        // only when completed and status code == 201 V
        if (xhttp.readyState == 4) {
            if (xhttp.status == 200) {
                let servletResp = JSON.parse(xhttp.responseText)
                document.getElementById('userBioHere').innerText = servletResp.userBio;
                document.getElementById('userImgHere').setAttribute("src", servletResp.filepath)
            }
        }
    }

    xhttp.open('post', DOMAIN + '/getUserByName')
    xhttp.setRequestHeader('Content-Type', 'application/json')
    xhttp.setRequestHeader('Accept', 'application/json')

    let obj = {
        "username": username
    }
    xhttp.send(JSON.stringify(obj));

}

function createPost() {
    s3upload("postUpload");

    // lol
    let input = document.getElementById("postUpload");
    let filename = input.value.split(/(\\|\/)/g).pop();
    let pictureUrl = "http://paperproj2.s3-website.us-east-2.amazonaws.com/" + filename;

    let xhttp = new XMLHttpRequest()

    xhttp.onreadystatechange = function () {
        // only when completed and status code == 201 V
        if (xhttp.readyState == 4) {
            if (xhttp.status == 201) {
                let servletResp = JSON.parse(xhttp.responseText)
                console.log(servletResp)
            }
        }
    }

    xhttp.open('post', DOMAIN + '/createPost')
    xhttp.setRequestHeader('Content-Type', 'application/json')
    xhttp.setRequestHeader('Accept', 'application/json')

    let user = {
        "username": username
    }

    let postObj = {
        "myUser": user,
        "postHeading": document.getElementById('postHeading').value,
        "postBody": document.getElementById('postBody').value,
        "postPic": pictureUrl
    }
    console.log(postObj)
    xhttp.send(JSON.stringify(postObj))
}

async function getPosts() {
    posts = await fetch(DOMAIN + '/getPostsByUsername', {
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
        method: "POST",
        body: JSON.stringify({ "username": username })
    }); //fetches posts from endpoint

    posts = await posts.json();

    return posts;
}

async function getPostComments(post) {
    const responsePayload = await fetch(DOMAIN + '/user/getAllComments', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Access-Control-Allow-Origin': '*' },
        body: JSON.stringify(post),
    });
    const responseJSON = await responsePayload.json();
    return responseJSON
}

function iterator(posts) {
    //ocument.getElementById("content_div").innerHTML = "";
    for (let i = 0; i < posts.length; i++)
        eachPost(posts[i], i);

    addCommentEventListeners()
}


//Creates each new post as a div, populates it and appends it to main
async function eachPost(post, ind) {
    if (typeof post === 'undefined') return null

    let comments = await getPostComments(post).then((result) => { return result })


    let allCommentsHTML = getCommentsHTML(comments)
    let postPicHTML = ''
    if (post.postPic !== null) postPicHTML = `<img class="rounded w-100 h-auto"  src="${post.postPic}" alt="Card image cap">`
    let newPost = document.createElement("div");

    newPost.innerHTML = `<div class ="card card-body rounded shadow w-100">
    <div class ="row">
       <div class = "col-4 align-items-center">
          ${postPicHTML}
       </div>
       <div class = "col-8">
          <div class = "row">
             <div id="postId"></div>
             <div id="postTitle">user: <a href="http://localhost:9876/html/ProfilePage.html?username=${post.myUser.username}">${post.myUser.username}</a>&nbsp;&nbsp;<strong>${post.postHeading}</strong>&nbsp;&nbsp;${post.postTimestamp}</div>
          </div>
          <div class = "row">
             <div id="postBody">${post.postBody}</div>
          </div>
       </div>
    </div>
    <div class="row">
       <div class ="col">
          <div class="form-check">
             <button class="btn btn-primary" style="margin-top:10px;margin-bottom:10px;" id="likePost-${ind}">Like Post</button>
          </div>
          <a class="btn btn-primary" data-toggle="collapse" href="#collapse-${ind}" role="button" aria-expanded="false" aria-controls="collapse-${ind}">
             View Comments
          </a>
       </div>
    </div>
    <div class="collapse" id="collapse-${ind}">
       <div class="">
          ${allCommentsHTML}<br>
          <h6>New Comment:</h6><br>
          <textarea id="commentBody-${ind}" name="commentBody-${ind}" rows="4" cols="50" style="resize: none;"></textarea><br><br>
          <button class="btn btn-primary" id="commentID-${ind}">Create Comment</button>
       </div>
    </div>
 </div>`;

    let selection = document.getElementById("content_div");
    selection.appendChild(newPost);

}

async function logout() {
    await fetch(DOMAIN + '/user/logout');
    sessionStorage.removeItem('userLogin');
    window.location.replace(DOMAIN + "/");
}

async function s3upload(htmlElement) {
    let input = document.getElementById(`${htmlElement}`);
    let file = input.files;
    let filename = input.value.split(/(\\|\/)/g).pop();
    let fileUrl = null;

    if (file) {
        file = file[0];
        fileUrl = "http://paperproj2.s3-website.us-east-2.amazonaws.com/" + filename;

        console.log(fileUrl)

        s3.upload({ Key: filename, Body: file }, function (err) {
            if (err) {
                console.log("error");
            }
            alert("Success");
        });
        return fileUrl;
    }
    return null;
}

function getCommentsHTML(comments) {
    let out = ``
    for (c of comments) {
        out = out.concat(`<div class="card" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">${c.user.username}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${c.commentTimestamp}</h6>
          <p class="card-text">${c.commentBody}</p>
        </div>
      </div>
      `)
    }
    return out
}

async function createComment(commentObj) {
    console.log(commentObj)
    const responsePayload = await fetch(DOMAIN + '/user/createComment', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'Accept': 'application/json', 'Access-Control-Allow-Origin': '*', "Access-Control-Allow-Credentials": true },
        body: JSON.stringify(commentObj),
    });
    window.location.replace(DOMAIN + "/explore");
}

function addCommentEventListeners() {
    for (i in posts) {
        let commentSubmitBtn = document.getElementById(`commentID-${i}`)
        commentSubmitBtn.addEventListener('click', submitComment)
        let likeSubmitBtn = document.getElementById(`likePost-${i}`)
        likeSubmitBtn.addEventListener('click', likePost)
        // console.log(likeSubmitBtn)
    }
}