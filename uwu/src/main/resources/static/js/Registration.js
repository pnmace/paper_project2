const DOMAIN = 'http://localhost:9876'

window.onload = function () {
    document.getElementById('createAccount').addEventListener('click', registerAcc)
}

function registerAcc() {
    let t = regHelp()
    if (!t) {
        console.log('bad info')
        return null
    } else console.log('valid info')
    let xhttp = new XMLHttpRequest()

    xhttp.onreadystatechange = function () {
        // only when completed and status code == 201
        if (xhttp.readyState == 4) {
            if (xhttp.status == 201) {
                let servletResp = JSON.parse(xhttp.responseText)
                console.log(servletResp)
                window.location.replace(DOMAIN + "/")
            } else if(xhttp.status == 409){
                alert('account with that username/email')
            }
        }
    }

    xhttp.open('post', DOMAIN + '/user/registration')
    xhttp.setRequestHeader('Content-Type', 'application/json')
    xhttp.setRequestHeader('Accept', 'application/json')

    let objToSend = {
        "username": document.getElementById('username').value,
        "userPassword": document.getElementById('userPassword').value,
        "userEmail": document.getElementById("userEmail").value
    }
    console.log(objToSend)
    xhttp.send(JSON.stringify(objToSend))
}

const validateEmail = (email) => {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
};

function regHelp() {
    if (document.getElementById('userPassword').value.length < 5) return false
    if (document.getElementById('username').value.length < 5) return false
    if (!validateEmail(document.getElementById('userEmail').value)) return false
    // console.log(validateEmail(document.getElementById('userEmail').value))
    let pass = document.getElementById('userPassword').value.localeCompare(document.getElementById('userPassword2').value) == 0
    return pass
}