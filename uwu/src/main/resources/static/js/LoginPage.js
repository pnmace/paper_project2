const DOMAIN = 'http://localhost:9876';

window.onload = function () {
    document.getElementById('LoginBtn').addEventListener('click', login); // controls the increment
    document.getElementById('ResetBtn').addEventListener('click', resetPassword);
    document.getElementById('SignUp').addEventListener('click', signUp);
}

function login() {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        // only when completed and status code == 200
        if (xhttp.readyState == 4) {
            if (xhttp.status == 200) {
                let servletResp = JSON.parse(xhttp.responseText)
                console.log(servletResp)
                sessionStorage.setItem('userLogin', servletResp.username);
                window.location.replace(DOMAIN + "/explore")
            } else if (xhttp.status == 401) {
                console.log('invalid creds');
                alert('Invalid Credentials');
            }
        }
    }

    xhttp.open('post', DOMAIN + '/user/login')
    xhttp.setRequestHeader('Content-Type', 'application/json')
    xhttp.setRequestHeader('Accept', 'application/json')

    let objToSend = {
        "username": document.getElementById('username').value,
        "userPassword": document.getElementById('password').value
    }
    xhttp.send(JSON.stringify(objToSend))
}

function resetPassword() {

    window.location.replace(DOMAIN + '/user/resetPassword');
    
}

function signUp() {

    window.location.replace(DOMAIN + '/user/registration');

}
