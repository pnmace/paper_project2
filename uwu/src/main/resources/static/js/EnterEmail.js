const DOMAIN = 'http://localhost:9876';
console.log("uwu is here");

window.onload = function() {

    document.getElementById('sendEmail').addEventListener('click', sendEmail);
    document.getElementById('backToLogin').addEventListener('click', backToLogin);

}

function sendEmail() {
    let xhttp = new XMLHttpRequest();
    
    
    xhttp.onreadystatechange = function () {
        // only when completed and status code == 200
        if (xhttp.readyState == 4) {
            if (xhttp.status == 200) {
                let servletResp = JSON.parse(xhttp.responseText);
                console.log(servletResp);
            } else if (xhttp.status == 500 | 509) {
                console.log('bad email');
                alert('Email address not found');
            }
        }
    }
    
    xhttp.open('POST', DOMAIN + '/user/resetPassword');
    xhttp.setRequestHeader('Content-Type', 'application/json');
    xhttp.setRequestHeader('Accept', 'application/json');
    
    let objToSend = {
        "userEmail": document.getElementById('email').value,
    }
    
    xhttp.send(JSON.stringify(objToSend));
    window.location.replace(DOMAIN + "/user/verification");
    
}

function backToLogin() {

    window.location.replace(DOMAIN + "/user/login");

}