const DOMAIN = 'http://localhost:9876';
console.log("uwu is here");

window.onload = function() {

    document.getElementById('change').addEventListener('click', changePassword);
    document.getElementById('backToLogin').addEventListener('click', backToLogin);
}

function verification() {
    let xhttp = new XMLHttpRequest();

    window.location.replace(DOMAIN + "/user/changePassword");

    xhttp.onreadystatechange = function () {
        // only when completed and status code == 200
        if (xhttp.readyState == 4) {
            if (xhttp.status == 200) {
                let servletResp = JSON.parse(xhttp.responseText);
                console.log(servletResp);
            } else if (xhttp.status == 401) {
                console.log('password mismatch');
                alert('Password mismatch');
            }
        }
    }

    xhttp.open('POST', DOMAIN + '/user/login');

    let objToSend = {
        "newPassword1": document.getElementById('userPassword1').value,
        "newPassword2": document.getElementById('userPassword2').value
    }
    xhttp.send(JSON.stringify(objToSend));


}

function backToLogin() {

    window.location.replace(DOMAIN + "/user/login");

}