const DOMAIN = 'http://localhost:9876';
console.log("uwu is here");

window.onload = function() {

    document.getElementById('verify').addEventListener('click', verification);
    document.getElementById('backToLogin').addEventListener('click', backToLogin);
}


function verification() {
    let xhttp = new XMLHttpRequest();

    
    xhttp.onreadystatechange = function () {
        // only when completed and status code == 200
        if (xhttp.readyState == 4) {
            if (xhttp.status == 200) {
                let servletResp = JSON.parse(xhttp.responseText);
                console.log(servletResp);
            } else if (xhttp.status == 500) {
                console.log('bad code');
                alert('Invalid token');
            }
        }
    }
    
    xhttp.open('POST', DOMAIN + '/user/verification');
    xhttp.setRequestHeader('Content-Type', 'application/json')
    xhttp.setRequestHeader('Accept', 'application/json')
    
    let objToSend = {
        "userVerification": document.getElementById('verificationCode').value,
    }
    xhttp.send(JSON.stringify(objToSend));
    window.location.replace(DOMAIN + "/user/changePassword");


}

function backToLogin() {

    window.location.replace(DOMAIN + "/user/login");

}