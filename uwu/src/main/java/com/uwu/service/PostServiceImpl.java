package com.uwu.service;

import java.sql.Timestamp;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uwu.dao.PostDao;
import com.uwu.model.Post;

@Service
@Transactional
public class PostServiceImpl implements PostService {
	
	@Autowired
	private PostDao repository;

	@Override
	public Post createPost(Post userPost) {

		System.out.println(userPost);
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		System.out.println(timestamp);
		
		userPost.setPostTimestamp(timestamp.toString());
		System.out.println(userPost);
		
		return repository.save(userPost);
		
	}

}
