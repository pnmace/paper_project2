package com.uwu.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uwu.dao.PasswordTokenDao;
import com.uwu.dao.UserDao;
import com.uwu.exception.IncorrectCredentialsException;
import com.uwu.exception.UserAlreadyExistException;
import com.uwu.model.PasswordResetToken;
import com.uwu.model.User;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao repository;
	
	@Autowired
	private PasswordTokenDao passwordRepo;
	
	@Override
	public User registerNewUserAccount(User user) throws UserAlreadyExistException {
		String email = user.getUserEmail();
		String username = user.getUsername();
		if(email == null || emailExists(email) || usernameExists(username)) {
			throw new UserAlreadyExistException("There is an acc with that email/username: " + email);
		}
		// create new user
		User u = new User();
		u.setUsername(user.getUsername());
		u.setUserBio("");
		u.setUserEmail(email);
		u.setUserPassword(user.getUserPassword());
		// default profile images
		u.setFilepath("https://headpat.xyz/uwu/QlhiVy.png");
		// save/return user info to db
		return repository.save(u);
	}
	
	@Override
	public User loginUserAccount(User user) throws IncorrectCredentialsException {
		User out = repository.findByUsernameAndUserPassword(user.getUsername(), user.getUserPassword());
		if(out == null) throw new IncorrectCredentialsException("Incorrect credentials given");
		return out;
	}

	/**
	 * helper function for user registration
	 * @param username of user to search for
	 * @return whether or not user with username exists
	 * @author Luis E
	 */
	private boolean usernameExists(String username) {
		return repository.findByUsername(username) != null;
	}

	/**
	 * helper function for user registration
	 * @param email of user to search for
	 * @return whether or not user with email exists
	 * @author Luis E
	 */
	private boolean emailExists(String email) {
		return repository.findByUserEmail(email) != null;
	}

	
	
	public User findUserByEmail(String userEmail) {
		return repository.findByUserEmail(userEmail);
	}

	public void createPasswordResetTokenForUser(User user, String token) {
		passwordRepo.save(new PasswordResetToken(user, token));
		
	}

	public User validatePasswordResetToken(String token) {
		return passwordRepo.findByToken(token);
	}

}
