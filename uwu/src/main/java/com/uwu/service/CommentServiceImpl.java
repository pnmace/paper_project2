package com.uwu.service;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uwu.dao.CommentDao;
import com.uwu.model.Comment;
import com.uwu.model.Post;
import com.uwu.model.User;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {

	@Autowired
	private CommentDao commentRepo;
	
	@Override
	public Comment insertComment(Comment c, User u) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//		Comment toSave = c;
		c.setUser(u);
		c.setCommentTimestamp(timestamp.toString());
		return commentRepo.save(c);
	}

	@Override
	public List<Comment> getAllCommentsByPost(Post p) {
		return commentRepo.findByPost(p);
	}

	
		
}
