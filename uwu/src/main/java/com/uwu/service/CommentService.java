package com.uwu.service;

import java.util.List;

import com.uwu.model.Comment;
import com.uwu.model.Post;
import com.uwu.model.User;

public interface CommentService {

	public Comment insertComment(Comment c, User u);
	public List<Comment> getAllCommentsByPost(Post p);
}
