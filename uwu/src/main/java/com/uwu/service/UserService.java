package com.uwu.service;

import com.uwu.exception.IncorrectCredentialsException;
import com.uwu.exception.UserAlreadyExistException;
import com.uwu.model.User;

public interface UserService {
	
	/**
	 * Creates a new account using given user
	 * @param User to be created
	 * @throws UserAlreadyExistException only if user has username or email that exists
	 * @author Luis E
	 */
	public User registerNewUserAccount(User user) throws UserAlreadyExistException;
	
	/**
	 * Checks credentials given
	 * @param User to be logged in as
	 * @throws IncorrectCredentialsException if credentials are invalid
	 * @author Luis E
	 */
	public User loginUserAccount(User user) throws IncorrectCredentialsException;
	
	/**
	 * @author Cameron A
	 * 
	 */
	public User findUserByEmail(String userEmail);
	
	public void createPasswordResetTokenForUser(User user, String token);
	
	public User validatePasswordResetToken(String token);
	
}
