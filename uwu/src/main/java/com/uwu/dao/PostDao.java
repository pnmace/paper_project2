package com.uwu.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uwu.model.Post;
import com.uwu.model.User;

public interface PostDao extends JpaRepository<Post, Integer> {
	
	List<Post> getPostsBymyUser(User user);

}
