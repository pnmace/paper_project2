package com.uwu.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uwu.model.PasswordResetToken;
import com.uwu.model.User;

public interface PasswordTokenDao extends JpaRepository<PasswordResetToken ,Integer> {

	User findByToken(String token);
	
	

}
