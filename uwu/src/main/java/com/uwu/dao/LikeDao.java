package com.uwu.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uwu.model.Like;
import com.uwu.model.Post;
import com.uwu.model.User;

public interface LikeDao extends JpaRepository<Like, Integer> {

	Like findByUserAndPost(User u, Post p);
	List<Like> findByPost(Post p);

}
