package com.uwu.dao;

import com.uwu.model.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserDao extends JpaRepository<User, Integer> {
	
	public User findByUsernameAndUserPassword(String username, String password);
	public User findByUserEmail(String email);
	public User findByUsername(String username);
	
}
