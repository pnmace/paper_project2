package com.uwu.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uwu.model.Comment;
import com.uwu.model.Post;

public interface CommentDao extends JpaRepository<Comment, Integer> {
	public List<Comment> findByPost(Post p);
}
