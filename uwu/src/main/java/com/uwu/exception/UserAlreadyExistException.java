package com.uwu.exception;

public class UserAlreadyExistException extends Exception {

	/**
	 * generated UID
	 */
	private static final long serialVersionUID = 6861419986363983942L;

	public UserAlreadyExistException(String message) {
		super(message);
	}
}
