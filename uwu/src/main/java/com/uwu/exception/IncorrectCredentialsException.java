package com.uwu.exception;

public class IncorrectCredentialsException extends Exception {

	/**
	 * generated UID
	 */
	private static final long serialVersionUID = -3340766102539140110L;
	
	public IncorrectCredentialsException(String message) {
		super(message);
	}
	
}
