package com.uwu.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Entity
@Table(name="users")
//@Getter @Setter @RequiredArgsConstructor @ToString @EqualsAndHashCode.


public class User {

	@Id
	@Column(name="userID", nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int userID;
	
	@Column(name="username", nullable=false, unique=true)
	private String username;
	
	@Column(name="userPassword", nullable=false)
	private String userPassword;
	
	@Column(name="userEmail", nullable=false, unique=true)
	private String userEmail;
	
	@Column(name="userBio", nullable=true)
	private String userBio;
	
	@Column(name="imageFilepath", nullable=false)
	private String filepath;
	
	//////////////
	@JsonIgnore
	@OneToMany(mappedBy="myUser", fetch=FetchType.LAZY)
	private List<Post> usersPosts = new ArrayList<>();
	
	@Override
	public String toString() {
		return "\n\tUser [userID=" + this.userID + ", username=" + this.username + ", password=" + this.userPassword + ", email="
				+ this.userEmail + ", posts=" + this.usersPosts + "]";
	}
	
}
