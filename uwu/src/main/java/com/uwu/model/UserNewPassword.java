package com.uwu.model;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class UserNewPassword {
	
	@NotNull
	private String newPassword1;
	
	@NotNull
	private String newPassword2;

}
