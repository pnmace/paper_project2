package com.uwu.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Entity
@Table(name="posts")
public class Post {
		
	@Id
	@Column(name="postID", nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int postID;

	///////////
	@ManyToOne(cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
	@JoinColumn(name="user_FK")
	private User myUser;

	@Column(name="postHeading", nullable=false)
	private String postHeading;

	@Column(name="postTimestamp", nullable=false)
	private String postTimestamp;
	
	@Column(name="postBody", nullable=false)
	private String postBody;

	@Column(name="postPic", nullable=true)
	private String postPic;
	
	//////////////
	@JsonIgnore
	@OneToMany(mappedBy="post", fetch=FetchType.LAZY)
	private List<Like> postLikes = new ArrayList<>();

	public Post(int postID, User myUser, String postHeading, String postBody, String postPic) {
		super();
		this.postID = postID;
		this.myUser = myUser;
		this.postHeading = postHeading;
		this.postBody = postBody;
		this.postPic = postPic;
	}
	
	@Override
	public String toString() {
		return "\n\tPost [postID=" + this.postID + ", userID=" + this.myUser.getUserID() + ", timestamp=" + this.postTimestamp + ", body="
				+ this.postBody + ", picPath=" + this.postPic + "]";
	}
	
}