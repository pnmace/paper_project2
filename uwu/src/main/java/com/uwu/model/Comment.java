package com.uwu.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Entity
@Table(name="comments")
public class Comment {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int commentID;
	
	@ManyToOne(cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
	@JoinColumn(name="user_FK")
	private User user;
	
	@ManyToOne(cascade=CascadeType.MERGE, fetch=FetchType.LAZY)
	@JoinColumn(name="post_FK")
	private Post post;
	
	@Column(name="commentTimestamp")
	private String commentTimestamp;
	
	@Column(name="commentBody")
	private String commentBody;
	
/*
 * might use this later but like
 * "not in the spec"
 * "mvc"
 * etc.
 */
//	@JsonIgnore
//	@OneToMany(mappedBy="post", fetch=FetchType.LAZY)
//	private List<Like> postLikes = new ArrayList<>();

}