package com.uwu.model;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class UserEmail {
	
	
	@NotNull
	private String userEmail;
	
	

}
