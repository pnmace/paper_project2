package com.uwu.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.uwu.dao.LikeDao;
import com.uwu.model.Like;
import com.uwu.model.Post;
import com.uwu.model.User;

@RestController
@CrossOrigin
public class LikeController {
	
	private LikeDao likeDao;

	@Autowired
	public LikeController(LikeDao likeDao) {
		this.likeDao = likeDao;
	}
	
	@PostMapping("/likePost")
	@ResponseStatus(HttpStatus.OK)
	public Like likePost(@RequestBody Like postLike, HttpServletRequest req) {
		User u = (User) req.getSession().getAttribute("user");
		if(u == null) return null;
		postLike.setUser(u);
		Like l = likeDao.findByUserAndPost(u, postLike.getPost());
		if(l != null) {
			likeDao.delete(l);
			return null;
		}
		return likeDao.save(postLike);
	}
	
	@PostMapping("/allLikesByPost")
	public List<Like> getLikes(@RequestBody Post p) {
		return likeDao.findByPost(p);
	}
	
	

}
