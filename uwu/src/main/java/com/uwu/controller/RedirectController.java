package com.uwu.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@CrossOrigin
public class RedirectController {

	/**
	 * this will be the timeline after the user logs in.
	 * 
	 * @author Parker M
	 */
	@GetMapping("/")
	public String showLoginpage(HttpServletRequest req) {
		
		if (req.getSession().getAttribute("user") != null) {
			return "redirect:/explore";
		}
		
		return "LoginPage";
	}
	/**
	 * this will be the timeline after the user logs in.
	 * 
	 * @author Parker M
	 */
	@GetMapping("/explore")
	public String showHomepage(HttpServletRequest req) {
		if (req.getSession().getAttribute("user") == null) {
			return "redirect:/";
		}
		return "ExplorePage";
	}
}
