package com.uwu.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.uwu.model.Comment;
import com.uwu.model.Post;
import com.uwu.model.User;
import com.uwu.service.CommentService;

@RestController
public class CommentController {
	
	private CommentService commentServ;

	@Autowired
	public CommentController(CommentService commentServ) {
		this.commentServ = commentServ;
	};
	
	@PostMapping("/user/createComment")
	public Comment createComment(@RequestBody Comment c, HttpServletRequest req) {
		User u = (User) req.getSession().getAttribute("user");
		if(u == null) return null;
		return commentServ.insertComment(c, u);
	}
	
	@PostMapping("user/getAllComments")
	public List<Comment> getAllCommentsByPost(@RequestBody Post p) {
		return commentServ.getAllCommentsByPost(p);
	}

}
