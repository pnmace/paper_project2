package com.uwu.controller;

import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.uwu.dao.UserDao;
import com.uwu.exception.IncorrectCredentialsException;
import com.uwu.exception.UserAlreadyExistException;
import com.uwu.model.User;
import com.uwu.model.UserEmail;
import com.uwu.model.UserVerification;
import com.uwu.service.UserServiceImpl;

// changed to controller to allow forwarding to html
@Controller
@CrossOrigin(origins = "*")
public class UserController {

	private UserDao userDao;
	private UserServiceImpl userService;
	private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	
	@Autowired
	public UserController(UserDao userDao, UserServiceImpl userService, JavaMailSender mailSender) {
		this.userDao = userDao;
		this.userService = userService;
	}

	// for testing DONT use in production
	@PostMapping("/createUser")
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody User createPost(@RequestBody User newUser) {
		userDao.save(newUser);

		return newUser;
	}

	/**
	 * this can be used to update most of the fields in the User object
	 * we will be updating the user's pfp using the "/updateUserPfp" endpoint
	 * 
	 * @apiNote	JSON params that can be updated:
	 * 			the user's Bio		@param "userBio"
	 * 			The user's Email 	@param "userEmail"
	 * 			The user's Password	@param "userPassword"
	 * 			The user's pfp url	@param "filepath"
	 * @author Parker M
	 */
	@PostMapping("/updateUser")
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody User updateUser(@RequestBody User userProfile) {

		User oldInfo = null;

		if (userProfile.getUsername() != null) oldInfo = userDao.findByUsername(userProfile.getUsername());
		else oldInfo = userDao.getById(userProfile.getUserID());

		if (userProfile.getUserBio() != null) oldInfo.setUserBio(userProfile.getUserBio());
		if (userProfile.getUserEmail() != null) oldInfo.setUserEmail(userProfile.getUserEmail());
		if (userProfile.getUserPassword() != null) oldInfo.setUserPassword(userProfile.getUserPassword());
		if (userProfile.getFilepath() != null) oldInfo.setFilepath(userProfile.getFilepath());

		userDao.save(oldInfo);

		return oldInfo;
	}
	
	@PostMapping("/getUserByName")
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody User getUserByName(@RequestBody User userName) {
		
		User user = userDao.findByUsername(userName.getUsername());
		
		System.out.println(user);
		
		return user;
	}

	// we might not even end up using this endpoint depending on how the s3 bucket
	// works
	@PostMapping("/updateUserPfp")
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody User updateUserPfp(@RequestBody User userProfile) {

		userDao.getById(userProfile.getUserID());

		return null;
	}

	/**
	 * method to forward users to registration.html
	 * 
	 * @return registration.html to be displayed to user
	 * @author Luis E
	 */
	@GetMapping("/user/registration")
	public String showRegistrationPage() {
		// change to the name of the html to forward user to
		return "RegisterAccount";
	}

	@PostMapping("/user/registration")
	public @ResponseBody User registerUserAccount(@RequestBody User user, HttpServletResponse resp) {
		User out = null;
		try {
			out = userService.registerNewUserAccount(user);
			// HTTP STATUS: created
			resp.setStatus(201);
		} catch (UserAlreadyExistException e) {
			// HTTP STATUS: conflict
			resp.setStatus(409);
		}
		return out;
	}

	/**
	 * method to forward users to login.html
	 * 
	 * @return login.html to be displayed to user
	 * @author Luis E
	 */
	@GetMapping("/user/login")
	public String showLoginPage() {
		return "LoginPage";
	}


	@PostMapping("/user/login")
	public @ResponseBody User loginUserAccount(@RequestBody User user, HttpServletRequest req,
			HttpServletResponse resp) {
		User out = null;
		try {
			out = userService.loginUserAccount(user);
			req.getSession().setAttribute("user", out);
			System.out.println("Successful Login");
		} catch (IncorrectCredentialsException e) {
			// HTTP STATUS: unauthorized
			resp.setStatus(401);
			System.out.println("Invalid Login: " + user.getUsername() + " " + user.getUserPassword());
		}

		return out;
	}

	@GetMapping("/user/resetPassword")
	public String showResetPasswordPage() {
		return "ResetPasswordEnterEmail";
	}
	
	/**
	 * 
	 * @author Cameron A
	 * If the entered email matches a user in the database a reset password email will be sent containing a code.
	 */
	@PostMapping("/user/resetPassword")
	public String resetPassword(HttpServletRequest req, HttpServletResponse resp, @RequestBody UserEmail userEmail)
			throws IncorrectCredentialsException, MessagingException {
		User acc = userService.findUserByEmail(userEmail.getUserEmail());
		if (acc == null) {
			resp.setStatus(509);
			throw new IncorrectCredentialsException("No Email found");
			
		}
		req.getSession().setAttribute("userEmail", acc.getUserEmail());
		String token = UUID.randomUUID().toString();
		userService.createPasswordResetTokenForUser(acc, token);
		
		String to = userEmail.getUserEmail();
		String from = "uwuBook@gmail.com";
		Properties properties = System.getProperties();
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.user", "uwuBook@gmail.com");
		properties.put("mail.smtp.port", 587);
		properties.put("mail.smtp.password", "po832bv7t3wnpqr");
		properties.put("mail.transport.protocol", "smtp");
	    properties.put("mail.smtp.auth", true);
	    properties.put("mail.smtp.starttls.enable", true);
	    Session session = Session.getDefaultInstance(properties);
		try {
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
		message.setSubject("Reset your uwuBook Password");
		message.setText("Please enter the code below into the verification page" + "\n\nVerification: " + token);
		Transport.send(message, "uwuBook@gmail.com", "po832bv7t3wnpqr");
		System.out.println("Email Sent");
		
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
		
		return "redirect:/ResetPasswordVerification";
	}
	
	@GetMapping("/user/verification")
	public String showVerificationPage() {
		return "ResetPasswordVerification";
	}
	
	/**
	 * 
	 * @author Cameron A
	 * 
	 */
	@PostMapping("/user/verification")
	public String verification(@RequestBody UserVerification token) throws IncorrectCredentialsException {
		User user = userService.validatePasswordResetToken(token.getUserVerification());
		System.out.println(user);
		if (user != null) {
			System.out.println("anything here");
			return "redirect:/NewPassword";
		}else {
			throw new IncorrectCredentialsException("Token entered is invalid");
		}
	}
	
	@GetMapping("/user/changePassword")
	public String changePassword() {
		return "NewPassword";
	}
	/**
	 * 
	 * @author Cameron A
	 */
	@PostMapping("/user/changePassword") 
	public String changePassword(HttpServletRequest req, @RequestParam("userPassword") String password1,
			@RequestParam("userPassword2") String password2) throws IncorrectCredentialsException {
		if (password1 != password2) {
			throw new IncorrectCredentialsException("Password mismatch");
		}
		else {
			User user = userService.findUserByEmail((String) req.getAttribute("userEmail"));
			user.setUserPassword(password1);
			return "redirect:/LoginPage";	
		}

	}
	
	@PostMapping("/user/logout")
	public String logoutUser(HttpServletRequest req) {
		
		req.getSession().invalidate();
		
		return "LoginPage";
		//return "LoginPage";
	}
}
