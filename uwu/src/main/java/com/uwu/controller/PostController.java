package com.uwu.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.uwu.dao.PostDao;
import com.uwu.dao.UserDao;
import com.uwu.model.Like;
import com.uwu.model.Post;
import com.uwu.model.User;
import com.uwu.service.PostServiceImpl;

@RestController
@CrossOrigin(origins = "*")
public class PostController {
	
	private PostDao postDao;
	private UserDao userDao;
	private PostServiceImpl postServ;

	@Autowired
	public PostController(PostDao postDao, UserDao userDao, PostServiceImpl postServ) {
		this.postDao = postDao;
		this.userDao = userDao;
		this.postServ = postServ;
	}
	
	/**
	 * @param userPost
	 * 			@param "myUser" <- this is an object
	 * 			@param "postHeading"
	 * 			@param "postBody"
	 * @author Parker M
	 */
	@PostMapping("/createPost")
	@ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody Post createPost(@RequestBody Post userPost) {
		
		User user = userDao.findByUsername(userPost.getMyUser().getUsername());
		
		userPost.setMyUser(user);
		
		postServ.createPost(userPost);

		return userPost;
	}
	
	@CrossOrigin
	@GetMapping("/getAllPosts")
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody List<Post> getAllPosts() {
		
		List<Post> allPosts = postDao.findAll();
		
		Collections.reverse(allPosts);
		
		System.out.println(allPosts);
		
		return allPosts;
	}
	
	@PostMapping("/getPostsByUsername")
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody List<Post> getPostsBymyUser(@RequestBody User currentUser) {

		// must send a user object containing the name in order to get the actual obj from the db
		User user = userDao.findByUsername(currentUser.getUsername());
		
		List<Post> posts = user.getUsersPosts();
		
		Collections.reverse(posts);
		
		return posts;
	}

	@GetMapping("/getPostLikes")
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody List<Like> getLikesByPostId(@RequestBody Post currentPost) {
		
		Post post = postDao.getById(currentPost.getPostID());
		
		List<Like> likes = post.getPostLikes();
		
		return likes;
	}
	
}
