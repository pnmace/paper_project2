package com.uwu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class ProfanityFilter {

	// splits a string into words based on space between words
	public static String[] splitStr(String s) {

		String[] splitStrArr;
		splitStrArr = s.toLowerCase().split(" ");
//	       System.out.println("The split array is: " + Arrays.toString(splitStrArr));   
		return splitStrArr;

	}

	public static boolean doesWcontainW(String fWord, String pWord) {

		int fwLen = fWord.length(); // length of foul word
		int pwLen = pWord.length(); // length of post word

		boolean match = false;

//			System.out.println("post word foul word ** " + pWord + " ***   " + fWord + "  *** \n\n");

		// which word is shorter?
		if (pwLen < fwLen) {
			if (fWord.contains(pWord)) { // postWord is shorter
				System.out.println("1 There is a match 1!");
				match = true;
			}
		}
		;

		if (pwLen >= fwLen) {
			if (pWord.contains(fWord)) { // postWord is shorter
				System.out.println("2 There is a match 2!");
				match = true;
			}
		}

		return match;

	}

	public static double getPercentMatch(String fWord, String pWord) {
			
			int fwLen = fWord.length();   // length of foul word
		    int pwLen = pWord.length(); // length of post word
		    int matchCount = 0;
		    double percMatch = 0;

			 
			int shorterLen = (pwLen > fwLen) ? fwLen : pwLen; 
			int longerLen = (pwLen > fwLen) ? pwLen : fwLen; 
			
//			System.out.println("Shorter length value: " + shorterLen); 
//			System.out.println("pwLen " + pwLen); 
//			System.out.println("********************************");

//			System.out.println("***********post word foul word ************* \n" + pWord + "     " + fWord + "  ");
			
			//compare each character of postWord with each character of pWord		
			for (int j = 0; j < shorterLen-1; j++) {
				if ((fWord.charAt(j) == pWord.charAt(j))) {
 
//					System.out.print(pWord.charAt(j));
//					System.out.print(" ");
					matchCount++; 
				}
			}
			if (matchCount > 0) {
				double mc = matchCount;
				double sl = shorterLen;
				percMatch = mc/sl;
			}
		    
//			System.out.println("percent match:     " + percMatch);
//			System.out.println("matchcount:     " + matchCount);
//			System.out.println("shorter length:     " + matchCount);
			
			return percMatch;
			
		}

	// method compares one foul word with words in post string to see if there is a
	// partial match at character level
	public static boolean foulWordY(String foulWord, String myPostStr) {
//			    System.out.println(myPostStr);
		String[] postStrArray = splitStr(myPostStr);
//			    System.out.println("post Str Array: " + postStrArray[0]);

		int postLen = myPostStr.length();
		String postWord = "";
		boolean containsFoul = false;

		// iterate over all words in post
		for (int i = 0; i < postLen - 1; i++) {
			if (i < 29) {
				postWord = postStrArray[i];
			} else
				break;

			System.out.println("***********post word foul word ************* \n" + " " + postWord + "   " + foulWord);
//					System.out.println(postStrArray[i] + " -- " + i);

			if ((postLen > 3) && (foulWord.length()) > 3) {
				System.out
						.println("***********post word foul word ************* \n" + " " + postWord + "   " + foulWord);
				if (doesWcontainW(foulWord, postWord)) {
					System.out.println("There is a match in does WcontainW! \n");
					containsFoul = true;
				}

				if ((getPercentMatch(foulWord, postWord)) > 0.8) {
					System.out.println("There is a percent match! \n");
					containsFoul = true;

				} else {

//							System.out.println("Post is safe to print");
					containsFoul = true;
				}
			}
		}
		return containsFoul;
	}

	public static void main(String[] args) {

		ArrayList<String> profanityArray = new ArrayList<String>(
				Arrays.asList("what the", "barnacles", "poop", "vomit", "xxx"));

		boolean profanityY = false;

		String postStr = "This is a long post that goes all over the place and has Whiteout conditions, "
				+ "strong winds and coastal flooding led several governors to and declare states of emergency.";

//		    postStr = String.join(" ",args);  // in case we want it entered from command line

		// check if any word in profanity array is in post or there is an 80% match
		for (String prof : profanityArray) {
//				System.out.println("In main prof: " + prof); 
			if (postStr.contains(prof)) {
				profanityY = true;
				System.out.println("Post violates profanity rules. Cannot post!  ");
				continue;
			} else if (foulWordY(prof, postStr)) {
				System.out.println(" ------ Post violates profanity rules. Cannot post! ------ ");
				profanityY = true;
			}
		}

		System.out.println("Profanity value is:  " + profanityY);

//			 if (profanityY)  
//				 System.out.println("\n ------ Post violates profanity rules. Cannot post! ------ \n");

	}
}
